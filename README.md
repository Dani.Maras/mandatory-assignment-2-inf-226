# Oblig 2 INF226 Part 2B Documentation
This project has been done by me, Dani Marås, but I've cooperated with other students when attending group sessions and at the study rooms at Høyteknologisenteret. Sorry to say, I don't know the names of every student that I've discussed matters with, but I will try to shed light on the ones that I know:

- Filip is part of a three man group. He helped me out with my log out button, as my original design for the button didn't work. 
- At Høyteknologisenteret I've discussed different matters with other students, although we haven't shared solutions. The most notably students in this regard are Simon Andersen and Theodor Mostad. I've also had discussions with some of the third year Computer Security students, but I don't know the names of all of these students. 
 

This README file is structured in the following manner
- a brief overview of my design considerations from Part A
- the features of my application
- instructions on how to test/demo it
- technical details on the implementation
- answers to the questions that we were provided

## A brief overview of my design considerations from part A

### Login and logout
By starting the server and accessing http://127.0.0.1:5000, we reach the login site. As we have direct access to the actual source code of the program, we also have direct access to all of the users and their respective passwords. However, an obvious problem with the setup of the page, is that it isn’t possible to log out. Due to the activation of a secret key, this means that by logging in as one of the users, you will stay logged in even if you close down the server. The next time you run the server, you will bypass the entire login-page, and go straight to http://127.0.0.1:5000/index.html. Obviously, this is a huge security flaw.

### User credentials and user registration
User credentials are stored in a dictionary “users”, where username acts as a key and its value is a new dictionary. In this new dictionary, password acts as key, and the value is a string representation of the password. Token is also a key in this dictionary, and its value is a string representation of the token. 

This way of storing user credentials is problematic. The reason is that access to these lines of code, makes it possible to obtain the passwords of the users, and thus full control over the profile of the user. This would apply for both employees with access to the source code, and hackers if they were able to obtain these lines.

Closely linked to the bad handling of user credentials, is the lack of user registration concerning app.py. The only way to register as a user in the original code, is to manually type in a new user in the dictionary ‘users’. This is problematic in a series of ways, but the three main concerns are 
- Whomever manually types it in will have access to the password of the user
- The passwords are not hashed or salted, so anyone with access to the database, will also have direct access to all username and passwords.
- The process is not automated, which makes it more prone for human errors.
- No requirement for unique usernames. You could add an identical user as one that already exists.

### SQL injection
Initially there exists a database named tiny. Sending messages, and searching for messages depends on sql queries, but there are no use of prepared statements in the original code. This means that a malign user can use sql injection, and for example erase the entire database. A malign user might also ruin Anya's reputation bu inputting:

'; UPDATE messages SET message='You suck' WHERE sender='Anya'; --

This would be a tragedy and needs to be avoided.

### Databases
The original database is set up with a database, tiny, and a dictionary. I have already commented on the dangers of implementing the user database as a dictionary, but I would also like to add the importance of streamlining the use of database design, to minimize the risk of human errors. In other words, the user database should be implemented in the same manner as tiny.

### Cross site scripting
The program is also exposed to cross site scripting attacks. None of the places that accepts user input have taken the steps to prevent users from inputting malign. This makes it possible for hackers to manipulate the website, as well as getting their hands on info inputted by the user. This could for example be username and password, by gaining access to the session cookie.

### Error messages
The program displays the sql queries it uses when sending/searching messages. This is probably done for our convenience when working with the program, but is a very bad idea as it provides potential hackers with information about how the database is set up.

### Accountability
The original layout of the applicaton is described as a type of Discord chat channel. However, it is not possible to send messages to a specific individual or group, but rather it acts like a forum where everyone can see what everyone has written. It is also possible to change the "from" field when writing a message. This allows users to write messages in other users names. This violates the accountability concerning usage of the service, and misuse of the service will prove impossible to trace.

### Structure/design
Our login-server-project is a good example of how a Flask application can be as simple as a single file. All though this is possible, it is not practical. By stacking the entire apllication's code (except for the login template) into one file, the application ends up losing transparency. This makes it harder for developers to read the code and recognizing the application's dependencies.  Thus, it will be harder to modify the structure and analyze for security flaws, and, alas, easier for developers to make human errors.

### Testing
There are no tests in the program. This makes it hard to verify that the program acts as the developer intends it to. It will also make it harder for developers who make changes to the program, to know if the changes will affect the security of the program.

## The features of my application
This report was written after the actual implementations, so there is a possibility that I forget to mention some of the changes that I've done. 

### Login and logout
I've added the possibility of logging out. This is represented as a button on the index page. Clicking the button logs you out, and redirects you to the login page.

### User credentials and user registration
I've expanded the "tiny" database to also hold the users and their credentials. The possibility of registration is also possible, and the need to verify the password is a demand (the users need to enter the password twice). The passwords are hashed and salted and stored in the password column in the user database. This means that an employee with access to the database will not be able to deduct the password of the user. The process of registering users is automated to reduce the possibility of human errors when manually adding users to the database (like in the original login-server-project). I've also made it impossible to create a user with an already existing username.

In order to hash and salt the passwords I imported the library bcrypt. I made an attempt at using the library WWWAuthenticate for hashing and salting, as this was already imported in the original login-server-project. However, I didn't get it to function properly.

### SQL injection
I've used prepared statements for all the sql queries in the code, to ensure that the database stays safe from maling users.

### Databases
I expanded the already existing tiny database with the table "users" and deleted the original table announcements. I also expanded the table messages with the columns receiver and datetime. Originally my plan was to make use of announcements, so that any message that was sent without a receiver would be put into announcements. However, I ended up with some troubles when it came to pretty-printing the result of two different SQL queries. When searching "*" I intended to get all announcements from the announcement table, as well as any messages where the current user is either sender or receiver. In order to use the functions already in place, I realized that the easiest way was to just make a user named "announcement" and make searching * also include messages for the user "announcement". In other words not an ideal solution but practical given the time limit of the assignment.

### XSS
I haven't had the time to try XSS attacks on the server properly due to lack of time, so I haven't been able to test whether the small changes I've tried actually enhanced the security of the app. After reading up on the subject, I was advised to change the use of innerHTML to innerText in the index.html file, to prevent users from having direct access to the cross siting that occurs while using the applications. This ended up affecting my pygmentize function, so that the sql queries performed by users when sending a message, became impossible to read. However, it is still possible to read the messages when searching or using the "Show all" button.

### Structure/design
I've divided the app.py file into several py-files, each with their own responsibility. The names should be self explanatory. I've also  made sure that each page has its own html file, and extracted the css code into their own files.

## Application features
The application is a forum of sorts, where users can post to the community by not specifying the receiver of the message, as well as post private messages to other users.

At the outset, the application has one sole user, "announcement", which allows for users to post announcements to the forum. However, it is possible to create new users. New users must have a unique username, and provide a password of at least 8 characters, with at least 1 number and one capital letter. I've based my password rules on Multipassword's security considerations: https://multipassword.com/en/articles/password-must-contain-letters-of-different-cases.

When a user logs in, he/she is automatically redirected from the "/login" page to the "/index.html" page. To ensure safety, a user that is logged in, can not not go back to the "/login" page without logging out. Trying to route your way over to "/login" while logged in, will redirect you to the safe haven of "/index.html". Here you can send messages to other users, and announcements to the community. Messages can be sent as private messages to other users by including a recipient in the "To" field of the index page. By leaving this field open, the message is sent to the user "announcement". By searchin "*" or clicking the "Show all", a user will be shown all messages he/she has sent or received, as well as all messages sent to "announcement". Note that it is also possible for a user to send messages to him/her self. I don't know of any security issues related to this, and schizophrenics are humans too!

After you have experienced the joy of communicating with other people, you can now log out of the application. "/index.html" has its own Logout button, which will log out the user and route you back to "/login". You can now enjoy the wonders of introversion, and not receiving any messages from the outside community.

## Instructions on how to test/demo it
The application is run in the same manner as the original login-server-project, but with one additional python package. Thus you need to install the python packages Flask, APSW, Pygments and bcrypt. Then you use the following command to start the server: 

python -m flask run

Now you should be able to find the server at http://localhost:5000/.

Initially the database shouldn't exist. However, when performing the flask run command it should be initialized as an empty database, save for the user "announcement". When creating a new user, the application will automatically turn the username into lower case letters. The same applies when logging in. This means that a username's unqiueness is ensured so that creating the user "anya" will prevent future users from creating the user "Anya" or vice versa. Note, however, that the same does not apply for the password, which is password sensitive.



## Technical details on the implementation
In addition to the libraries used in the original login-server-project, this project also makes use of the bcrypt library. This is used to hash and salt passwords when creating a new user, and when a user tries to log in.


## Questions
### Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

The original login-server-project is very simple in its design, and allows a malignant user to exploit it in several different ways. The initial login function didn't have any password verification, which meant that it was possible to log in simply by inputting an existing user name. Even though you were logged in, it was also possible to enter "/login" in the address line, and log in as a different user. I never checked how this affected the current_user status, and I don't have the time to check it now. However, it is safe to say that it is a dire security concern.

The original project also lacked a logout function. This means that a logged in user might close down the server, thinking that he/she has logged out, but actually staying logged in. This will allow the next person to connect to the server to act as the former user.

SQL injections are also a threat. Both the search function and the messaging function of the index.html allows for injections and has no use of prepared statements. This means that a malignant user can manipulate the database in more or less any fashion he/she might want. In this regard it is also worth noticing that a user originally could choose the recipient of the message. However, even if you removed the "from" field in the original project, sql injections would still allow malignant users to create messages in other user's names. Or for that matter delete the entire database.

In the original project, the user database was implemented as a dictionary. I've already addressed the security issues concerning this:
- Whomever manually types it in will have access to the password of the user
- The passwords are not hashed or salted, so anyone with access to the database, will also have direct access to all username and passwords.
- The process is not automated, which makes it more prone for human errors.
- No requirement for unique usernames. You could add an identical user as one that already exists.

XSS are possible in the original project. In the "index.html" file, the user can directly input into fields using inner.HTML allowing for such attacks.

CSRF attacks are also possible with the original project, as there is no prevention from this whatsoever.

With regards to the CIA triad, all angles of the triad can quite easily be breached. Here are some examples.
- Confidentiality can be breached by untrustworthy employees with access to a user database with unhashed passwords. As it is originally not possible to log out, you might also exit the server only for the next person connecting to the server to have full access to your profile.
- Integrity can be breached by sendins a message and inputting a different user than yourself. Another way is to manipulate the database using SQL injections.
- Availability can most importantly be breached by dropping the table using SQL inection.

I would assume that the limits of what an attacker can do, are set by the limits of the application. The malignant user can manipulate the databases in a wide variety of ways, but I don't know of anything else that can be done outside manipulating/ruining the application.

The limits of what we sensibly can protect against are set by the limits of the attacks that we are already aware of. I assume that it is fully possible to attack this application in a manner that I haven't thought of.

### What are the main attack vectors for the application?
The main attack vector is through the "/index.html" page which allows for sql injections, xss attacks and csrf attacks. 

### What should we do (or what have you done) to protect against attacks?
The first thing I did was to implement a log out function, to make sure that logging in didn't mean staying logged in forever.
Then I expanded the tiny database with a user table, to streamline the architecture of the project. I also made sure that the user passwords were securely hashed and salted, to prevent passwords from being stored as plain text. 

The main attack vector is the "/index.html" page. To prevent SQL injections I've used prepared statements throughout the app, where the user can input text used in sql queries. I've also removed the "from" field on the "/index.html" page. In the actual send function, the current user is set automatically as being who the message is from.

To prevent xss one should change the index.html file so that innerText is used instead of innerHTML. However, by doing this, I ended up ruining the pygmentize function when outputting sent messages. It is still possible to read the messages by searching or clicking the "Show all" button.

CSRF attacks. I haven't had time to implement this yet. There should be taken steps to ensure that the person performing an action on the index.html page is the same as the current logged in user. 

### What is the access control model?
In this application I interpret the messages as being objects, whereas the sender of the message is the owner of the object. This owner controls the the permissions to access this object, by choosing to whomever he/she sends the message. Due to this, the access model adheres to the discretionary access model as  "the controls are discretionary in the sense that a subject with a certain access permission is capable of passing that permission (perhaps indirectly) on to any other subject" https://en.wikipedia.org/wiki/Discretionary_access_control.

### How can you know that you security is good enough?
We can only prevent attacks that we already know of, so a minimal security design should aim for the prevention of these. However, although is impossible to know that your security is good enough, one should take steps so that potential attacks can be treated. One of these steps is to ensure traceability. This is done in order to identify security incidents and monitor policy violations, so that it is possible for developers to obtain informationabout problems and unusual conditions https://cheatsheetseries.owasp.org/cheatsheets/Logging_Cheat_Sheet.html.






