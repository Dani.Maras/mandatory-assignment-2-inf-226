from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired

class LoginForm(FlaskForm):
    # Unique username handled in login_and_register.
    username = StringField('Username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])
    submit = SubmitField('Submit')

class RegisterForm(FlaskForm):
    # Password must be entered twice in order to ensure correct typing.
    username = StringField('Username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])
    confirm_password = PasswordField('PasswordConfirm', validators=[InputRequired()])
    submit = SubmitField('Submit')
