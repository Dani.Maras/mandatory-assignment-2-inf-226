from users import hash_password
from app import app, conn
from users import user_loader, verify_password, verify_password_rules

import flask
from flask import abort, request, send_from_directory, make_response, render_template
from forms import LoginForm, RegisterForm
from flask_login import login_required, login_user, logout_user, current_user

# http://127.0.0.1:5000/search?q=%2A
# <script>alert('hi')</script>
# <img src onerror="alert('hi')">

@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('index.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        password_confirm = form.confirm_password.data
        username_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (username,)).fetchone()
        # Check if username is unique, passwords match and if rules are followed.
        if username_exists:
            print(f'The username "{username}" is already in use.')

        elif password != password_confirm:
            print('Passwords do not match.')

        elif not verify_password_rules(password):
            print('Password needs to have at least eight characters, one capital letter and a number.')

        else:
            password_hash = hash_password(password)
            conn.cursor().execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password_hash.decode("utf-8")))
            print(f"User {username} has been created.")
            return flask.redirect(flask.url_for('login'))

    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    # Prevents several users from logging in at once. If user is already logged in and tries access the login page, redirect to index.
    if current_user.is_authenticated: 
        return flask.redirect(flask.url_for('index_html'))
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data

        username_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (username,)).fetchone()

        if username_exists:
            password_hash = conn.cursor().execute('SELECT password FROM users WHERE username=?', (username,)).fetchone()[0]

        if username_exists and verify_password(password, password_hash):
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('login.html', form=form)

@app.get('/logout')
@login_required
def logout():
    logout_user()
    #return render_template('login.html', form=form)
    return flask.redirect(flask.url_for('login'))