from flask import Flask
import sys
import apsw
from apsw import Error
from bcrypt import gensalt, hashpw




# Setup app
app = Flask(__name__, template_folder="templates")
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!) #TODO
app.secret_key = 'mY s3kritz'

# Connect to the database
try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        message TEXT NOT NULL,
        receiver TEXT NOT NULL,
        datetime DATETIME NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        token TEXT);''')
    c.execute(f'INSERT INTO users (username, password, token) VALUES ("announcement", "{hashpw("announcements".encode("utf-8"), gensalt()).decode("utf-8")}", "")')


except Error as e:
    print(e)
    sys.exit(1)

import index_actions
import login_and_register
import coffee
