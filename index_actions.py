from app import app, conn
from pygmentize import pygmentize

import flask
from flask import abort, request, send_from_directory, make_response, render_template
from flask_login import login_required
from markupsafe import escape
from json import dumps, loads
from flask_login import current_user
from apsw import Error
from datetime import datetime

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'static/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'static/favicon.png', mimetype='image/png')

@app.get('/search')
@login_required
def search():
    query = request.args.get('q') or request.form.get('q')

    if query == '*':
        stmt = f"SELECT * FROM messages WHERE sender GLOB ? OR receiver GLOB ? OR RECEIVER GLOB 'announcement'"
        params = (current_user.id, current_user.id)
        prettyPrint = f"SELECT * FROM messages WHERE sender GLOB {current_user.id} OR receiver GLOB {current_user.id}"
        result = f"Query: {pygmentize(prettyPrint)}\n"
    else:
        stmt = f"SELECT * FROM messages WHERE message GLOB ? AND (sender GLOB ? OR receiver GLOB ?)"
        params = (query, current_user.id, current_user.id)
        prettyPrint = f"SELECT * FROM messages WHERE message GLOB {query} AND WHERE sender GLOB {current_user.id} OR receiver GLOB {current_user.id}"
        result = f"Query: {pygmentize(prettyPrint)}\n"
    try:
        c = conn.cursor().execute(stmt, params)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)


@app.route('/send', methods=['POST','GET'])
@login_required
def send():
    # By not adding a receiver, the message turns into an announcement for everybody.
    receiver = request.args.get('receiver') or request.form.get('receiver')
    sender = current_user.id
    message = request.args.get('message') or request.form.get('message')       
    now = datetime.now()
    # Check whether the receiver of the message is an actual user.
    # Empty messages are not allowed
    if not message:
        return f'ERROR: missing message'
    # If the receiver field is empty, the message is treated as an announcement for everybody
    if not receiver:
        receiver = "announcement"
    try:
        username_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (receiver,)).fetchone()
        if not username_exists:
            return f'ERROR: User {receiver} does not exist.'
        stmt = 'INSERT INTO messages (sender, message, receiver, datetime) VALUES (?, ?, ?, ?)'
        params = (sender,  message, receiver, str(now))
        conn.cursor().execute(stmt, params)
        prettyPrint = f'INSERT INTO messages (sender, message, receiver, datetime) VALUES ({sender}, {message}, {receiver}, {now})'
        result = f"Query: {pygmentize(prettyPrint)}\n"
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'
        






